#!/usr/bin/env ruby

# 3-D gear wheels. This program is in the public domain.
#
# Command line options:
#    -info      print GL implementation information
#    -exit      automatically exit after 30 seconds
#
# 2009-Mar-08 Modified Ruby version by Michael Brooks changed to demonstrate 
#             using GLSL shaders to implement velvet and shiny materials.
# 2005-May-01 Ruby version by Arto Bendiken based on gears.c rev 1.8.
# 2005-Jan-09 Original C version (gears.c) by Brian Paul et al.
#             http://cvs.freedesktop.org/mesa/Mesa/progs/demos/gears.c?rev=1.8

require 'opengl'
require 'glut'

class Gears

  include Math

  # Define constants
  LIGHT_POS = [100.0, 0.0, 0.0, 1.0]
  RED = [0.8, 0.1, 0.0, 1.0]
  GREEN = [0.0, 0.8, 0.2, 1.0]
  BLUE = [0.2, 0.2, 1.0, 1.0]

  def create_gear(inner_radius, outer_radius, width, teeth, tooth_depth)
    # Draw a gear wheel based on the following inputs:
    #
    #   inner_radius - radius of hole at center
    #   outer_radius - radius at center of teeth
    #   width - width of gear
    #   teeth - number of teeth
    #   tooth_depth - depth of tooth

    r0 = inner_radius
    r1 = outer_radius - tooth_depth / 2.0
    r2 = outer_radius + tooth_depth / 2.0

    da = 2.0 * PI / teeth / 4.0

    GL.Normal(0.0, 0.0, 1.0)

    # Draw front face
    GL.Begin(GL::QUAD_STRIP)
      for i in 0..teeth
        angle = i * 2.0 * PI / teeth
        GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), width * 0.5)
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), width * 0.5)
        if i < teeth
          GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), width * 0.5)
          GL.Vertex3f(r1 * cos(angle + 3 * da),
            r1 * sin(angle + 3 * da), width * 0.5)
        end
      end
    GL.End()
  
    # Draw front sides of teeth
    GL.Begin(GL::QUADS)
      for i in 0...teeth
        angle = i * 2.0 * PI / teeth
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), width * 0.5)
        GL.Vertex3f(r2 * cos(angle + da), r2 * sin(angle + da), width * 0.5)
        GL.Vertex3f(r2 * cos(angle + 2 * da),
          r2 * sin(angle + 2 * da), width * 0.5)
        GL.Vertex3f(r1 * cos(angle + 3 * da),
          r1 * sin(angle + 3 * da), width * 0.5)
      end
    GL.End()
  
    GL.Normal(0.0, 0.0, -1.0)
  
    # Draw back face
    GL.Begin(GL::QUAD_STRIP)
      for i in 0..teeth
        angle = i * 2.0 * PI / teeth
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), -width * 0.5)
        GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), -width * 0.5)
        if i < teeth
          GL.Vertex3f(r1 * cos(angle + 3 * da),
            r1 * sin(angle + 3 * da), -width * 0.5)
          GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), -width * 0.5)
        end
      end
    GL.End()
  
    # Draw back sides of teeth
    GL.Begin(GL::QUADS)
      for i in 0...teeth
        angle = i * 2.0 * PI / teeth
        GL.Vertex3f(r1 * cos(angle + 3 * da),
          r1 * sin(angle + 3 * da), -width * 0.5)
        GL.Vertex3f(r2 * cos(angle + 2 * da),
          r2 * sin(angle + 2 * da), -width * 0.5)
        GL.Vertex3f(r2 * cos(angle + da), r2 * sin(angle + da), -width * 0.5)
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), -width * 0.5)
      end
    GL.End()
  
    # Draw outward faces of teeth
    GL.Begin(GL::QUAD_STRIP)
      for i in 0...teeth
        angle = i * 2.0 * PI / teeth
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), width * 0.5)
        GL.Vertex3f(r1 * cos(angle), r1 * sin(angle), -width * 0.5)
        u = r2 * cos(angle + da) - r1 * cos(angle)
        v = r2 * sin(angle + da) - r1 * sin(angle)
        len = sqrt(u * u + v * v)
        u /= len
        v /= len
        GL.Normal(v, -u, 0.0)
        GL.Vertex3f(r2 * cos(angle + da), r2 * sin(angle + da), width * 0.5)
        GL.Vertex3f(r2 * cos(angle + da), r2 * sin(angle + da), -width * 0.5)
        GL.Normal(cos(angle), sin(angle), 0.0)
        GL.Vertex3f(r2 * cos(angle + 2 * da),
          r2 * sin(angle + 2 * da), width * 0.5)
        GL.Vertex3f(r2 * cos(angle + 2 * da),
          r2 * sin(angle + 2 * da), -width * 0.5)
        u = r1 * cos(angle + 3 * da) - r2 * cos(angle + 2 * da)
        v = r1 * sin(angle + 3 * da) - r2 * sin(angle + 2 * da)
        GL.Normal(v, -u, 0.0)
        GL.Vertex3f(r1 * cos(angle + 3 * da),
          r1 * sin(angle + 3 * da), width * 0.5)
        GL.Vertex3f(r1 * cos(angle + 3 * da),
          r1 * sin(angle + 3 * da), -width * 0.5)
        GL.Normal(cos(angle), sin(angle), 0.0)
      end
      GL.Vertex3f(r1 * cos(0), r1 * sin(0), width * 0.5)
      GL.Vertex3f(r1 * cos(0), r1 * sin(0), -width * 0.5)
    GL.End()
  
    # Draw inside radius cylinder
    GL.Begin(GL::QUAD_STRIP)
      for i in 0..teeth
        angle = i * 2.0 * PI / teeth
        GL.Normal(-cos(angle), -sin(angle), 0.0)
        GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), -width * 0.5)
        GL.Vertex3f(r0 * cos(angle), r0 * sin(angle), width * 0.5)
      end
    GL.End()
  end

  def create_shader_program(shader_base_file_name)
    # Create GLSL program and shader objects
    glsl_program = GL.CreateProgram()
    glsl_vertex_shader = GL.CreateShader(GL_VERTEX_SHADER)
    glsl_fragment_shader = GL.CreateShader(GL_FRAGMENT_SHADER)

    # Load shader source into the shaders
    GL.ShaderSource(glsl_vertex_shader,
                    File.read(File.join(Dir.getwd, 
                                        "#{shader_base_file_name}.vsh")))
    GL.ShaderSource(glsl_fragment_shader, 
                    File.read(File.join(Dir.getwd, 
                                        "#{shader_base_file_name}.fsh")))

    # Compile shaders
    GL.CompileShader(glsl_vertex_shader)
    GL.CompileShader(glsl_fragment_shader)

    # Attach the shaders to the program
    GL.AttachShader(glsl_program, glsl_vertex_shader)
    GL.AttachShader(glsl_program, glsl_fragment_shader)

    # Link the program
    GL.LinkProgram(glsl_program)
    
    # Cleanup the shaders, the program copied them so they aren't needed anymore
    GL.DeleteShader(glsl_vertex_shader)
    GL.DeleteShader(glsl_fragment_shader)
    
    # Pass the GLSL program back to the caller
    return(glsl_program)
  end
  
  def setup_scene()
    # Setup rotation tracking variables
    @angle = 0.0
    @view_rotx, @view_roty, @view_rotz = 20.0, 30.0, 0.0

    # Setup OpenGL environment
    GL.Enable(GL::DEPTH_TEST)
    GL.Enable(GL::CULL_FACE)
    GL.Lightfv(GL::LIGHT0, GL::POSITION, LIGHT_POS)
    GL.Enable(GL::LIGHT0)
    GL.ShadeModel(GL::SMOOTH)
    GL.Enable(GL::NORMALIZE)

    # Create the gear objects 
    # Note: Includes some material attributes which the GLSL shaders 
    #       can use or ignore at their disgression.
    @gear1 = GL.GenLists(1)
      GL.NewList(@gear1, GL::COMPILE)
      GL.Material(GL::FRONT, GL::AMBIENT_AND_DIFFUSE, RED)
      GL.Material(GL::FRONT, GL::SPECULAR, [1.0, 1.0, 1.0]);
      GL.Material(GL::FRONT, GL::SHININESS, 50);
      create_gear(1.0, 4.0, 1.0, 20, 0.7)
    GL.EndList()

    @gear2 = GL.GenLists(1)
      GL.NewList(@gear2, GL::COMPILE)
      GL.Material(GL::FRONT, GL::AMBIENT_AND_DIFFUSE, GREEN)
      GL.Material(GL::FRONT, GL::SPECULAR, [1.0, 1.0, 1.0]);
      GL.Material(GL::FRONT, GL::SHININESS, 50);
      create_gear(0.5, 2.0, 2.0, 10, 0.7)
    GL.EndList()

    @gear3 = GL.GenLists(1)
      GL.NewList(@gear3, GL::COMPILE)
      GL.Material(GL::FRONT, GL::AMBIENT_AND_DIFFUSE, BLUE)
      GL.Material(GL::FRONT, GL::SPECULAR, [1.0, 1.0, 1.0]);
      GL.Material(GL::FRONT, GL::SHININESS, 50);
      create_gear(1.3, 2.0, 0.5, 10, 0.7)
    GL.EndList()

    # Create the shader programs
    # Note: These shaders are based on code provided by others which has been
    #       slightly cleaned up or modified to better meet the needs of this
    #       demonstration program.
    @glsl_program1 = create_shader_program('shiny')
    @glsl_program2 = create_shader_program('velvet')
  end

  def cleanup_scene()
    # Delete OpenGL objects that Ruby doesn't know to clean up for us.
    # Note: This doesn't get called yet.  See note in Gears.finalize method.
    GL.DeleteProgram(@glsl_program1)
    GL.DeleteProgram(@glsl_program2)
    GL.DeleteLists(@gear1, 1)
    GL.DeleteLists(@gear2, 1)
    GL.DeleteLists(@gear3, 1)
  end
  
  def draw_scene()
    # Clear scene
    GL.Clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT);

    # Save current scene rotation
    GL.PushMatrix()
    
    # Rotate scene
    GL.Rotate(@view_rotx, 1.0, 0.0, 0.0)
    GL.Rotate(@view_roty, 0.0, 1.0, 0.0)
    GL.Rotate(@view_rotz, 0.0, 0.0, 1.0)

    # Activate shader program 1
    GL.UseProgram(@glsl_program1)

    # Draw gear 1
    GL.PushMatrix()
    GL.Translate(-3.0, -2.0, 0.0)
    GL.Rotate(@angle, 0.0, 0.0, 1.0)
    GL.CallList(@gear1)
    GL.PopMatrix()

    # Activate shader program 2
    GL.UseProgram(@glsl_program2)

    # Draw gear 2
    GL.PushMatrix()
    GL.Translate(3.1, -2.0, 0.0)
    GL.Rotate(-2.0 * @angle - 9.0, 0.0, 0.0, 1.0)
    GL.CallList(@gear2)
    GL.PopMatrix()

    # Draw gear 3
    GL.PushMatrix()
    GL.Translate(-3.1, 4.2, 0.0)
    GL.Rotate(-2.0 * @angle - 25.0, 0.0, 0.0, 1.0)
    GL.CallList(@gear3)
    GL.PopMatrix()

    # Restore "saved" scene rotation
    GL.PopMatrix()

    # Make the new scene visible in the window
    GLUT.SwapBuffers()

    # Calculate the drawing performance and display it every 5 seconds
    @frames = 0 if not defined?(@frames)
    @t0 = 0 if not defined?(@t0)
    @frames += 1
    t = GLUT.Get(GLUT::ELAPSED_TIME)
    if t - @t0 >= 5000
      seconds = (t - @t0) / 1000.0
      fps = @frames / seconds
      printf("%d frames in %6.3f seconds = %6.3f FPS\n", @frames, seconds, fps)
      @t0, @frames = t, 0
      exit if defined?(@autoexit) and t >= 999.0 * @autoexit
    end
  end

  def handle_window_resize(width, height)
    # Handle new window size or exposure
    h = height.to_f / width.to_f
    GL.Viewport(0, 0, width, height)
    GL.MatrixMode(GL::PROJECTION)
    GL.LoadIdentity()
    GL.Frustum(-1.0, 1.0, -h, h, 5.0, 60.0)
    GL.MatrixMode(GL::MODELVIEW)
    GL.LoadIdentity()
    GL.Translate(0.0, 0.0, -40.0)
  end

  def handle_window_visibilitychange(visibility)
    # Handle changes in the visibility of the window
    GLUT.IdleFunc((visibility == GLUT::VISIBLE ? method(:handle_idling).to_proc : nil))
  end

  def handle_keypress(k, x, y)
    # Handle keyboard requests for Z view angle change or exit 
    case k
      when ?z # pressed 'z', inc Z rotation
        @view_rotz += 5.0
      when ?Z # pressed 'z', dec Z rotation
        @view_rotz -= 5.0
      when 27 # pressed escape, exit
        exit
    end
    GLUT.PostRedisplay()
  end

  def handle_special_keypress(k, x, y)
    # Handle keyboard requests for X and Y view angle change 
    case k
      when GLUT::KEY_UP # pressed up-arrow, inc X rotation
        @view_rotx += 5.0
      when GLUT::KEY_DOWN # pressed down-arrow, dec X rotation
        @view_rotx -= 5.0
      when GLUT::KEY_LEFT # pressed left-arrow, inc Y rotation
        @view_roty += 5.0
      when GLUT::KEY_RIGHT # pressed right-arrow, dec Y rotation
        @view_roty -= 5.0
    end
    GLUT.PostRedisplay()
  end

  def handle_mouse_click(button, state, x, y)
    # Record mouse position when buttons are first clicked to support rotation calc. 
    @mouse = state
    @x0, @y0 = x, y
  end
  
  def handle_mousedown_motion(x, y)
    # Handle mouse requests for X and Y view angle changes. 
    if @mouse == GLUT::DOWN
      @view_roty -= @x0 - x
      @view_rotx -= @y0 - y
    end
    @x0, @y0 = x, y
  end

  def handle_idling()
    # Handle additonal tasks in between GLUT drawing events...
    
    # - Record the current GLUT ticks for later use in perofmance calc 
    t = GLUT.Get(GLUT::ELAPSED_TIME) / 1000.0
    @t0_idle = t if !defined?(@t0_idle)
    
    # - Tell the gears to rotate 90 degrees per second
    @angle += 70.0 * (t - @t0_idle)
    @t0_idle = t
    GLUT.PostRedisplay()
  end

  def handle_command_line_parameters()
    # Handle the following command line parameters:
    # 
    #   -info : Prints OpenGL driver information.
    #   -exit : Forces app to run for 30 seconds then exit. 
    
    ARGV.each do |arg|
      case arg
        when '-info'
          printf("GL_RENDERER   = %s\n", GL.GetString(GL::RENDERER))
          printf("GL_VERSION    = %s\n", GL.GetString(GL::VERSION))
          printf("GL_VENDOR     = %s\n", GL.GetString(GL::VENDOR))
          printf("GL_EXTENSIONS = %s\n", GL.GetString(GL::EXTENSIONS))
        when '-exit'
          @autoexit = 30
          printf("Auto Exit after %i seconds.\n", @autoexit);
      end
    end
  end
  
  def initialize()
    # Exit if the environment can't handle the OpenGL 2.0 style 
    # GLSL shader api commands
    if not (GL.respond_to?('CreateProgram') and
            GL.respond_to?('CreateShader') and
            GL.respond_to?('ShaderSource') and
            GL.respond_to?('CompileShader') and
            GL.respond_to?('AttachShader') and
            GL.respond_to?('LinkProgram') and
            GL.respond_to?('DeleteShader') and
            GL.respond_to?('DeleteProgram'))
      puts('Error - The program has terminated because the environment does ' +
           'not support OpenGL 2.0 or greater GLSL shaders.')
      exit
    end

    # Setup OpenGL environment
    GLUT.Init()
    GLUT.InitDisplayMode(GLUT::RGBA | GLUT::DEPTH | GLUT::DOUBLE)
    GLUT.InitWindowPosition(0, 0)
    GLUT.InitWindowSize(300, 300)
    GLUT.CreateWindow('Gears')

    # Handle command line parameters
    handle_command_line_parameters()
    
    # Setup scene
    setup_scene()

    # Setup GLUT event handlers
    GLUT.DisplayFunc(method(:draw_scene).to_proc)
    GLUT.ReshapeFunc(method(:handle_window_resize).to_proc)
    GLUT.VisibilityFunc(method(:handle_window_visibilitychange).to_proc)
    GLUT.KeyboardFunc(method(:handle_keypress).to_proc)
    GLUT.SpecialFunc(method(:handle_special_keypress).to_proc)
    GLUT.MouseFunc(method(:handle_mouse_click).to_proc)
    GLUT.MotionFunc(method(:handle_mousedown_motion).to_proc)
  end

  def Gears.finalize()
    # Cleanup the OpenGL objects
    # Note: Haven't found a way to get this finalize method called in a  
    #       standard GLUT environment when closing the applications.
    cleanup_scene()
  end
  
  def start()
    # Pass control to GLUT to display and manage the scene
    GLUT.MainLoop()
  end

end

# *** Program execution starts here ***
Gears.new().start() 

