Created 2009-Mar-08 by Michael Brooks:


--- Description ---

This Ruby program uses OpenGL, Glut and GLSL shaders to displays 3 different
colored mechanical gears rotating at about 90 degrees per second.  The program
is based heavily on the Ruby "gears.rb" program created by Arto Bendiken in May,
2005.  The program differs from the original program in the following ways:

- Renamed the original procedures and added comments to, hopefully, 
  add clarity to the code.
- Reorganized some of the original code around to, hopefully, 
  add clarity to the code.
- Changed the flat lit red, green and blue gears to two velvet (green and blue) 
  gears and one shiny (red) gear using a combination of standard OpenGL material 
  definitions and two GLSL shaders.
- Disabled (i.e. didn't enable) standard OpenGL lighting and smoothing because 
  the shaders take care of that.

The example has been testing with Ruby 1.8.6 on an Intel Pentium P4 3Ghz 
equipped with an overclocked AGP ATI Radeon 3850 with Windows XP and running 
at about 1850 fps.  It performs at the same speed as the original gears.rb
program but displays more complex materials.  I haven't yet successfully 
installed ruby-opengl in Ruby 1.9.1 on my PC but see no reason why this 
program couldn't run on Ruby 1.9.1.

On a side note, if you change the program to use only one of the two shaders 
it will run at about 2150 fps, which demonstrates the penalty for "state changes"
in OpenGL (the same kind of thing can happen in Direct3D too).

Please note, this program is not meant to teach GLSL shaders... that is a much 
more complex topic.  It is meant to help folks get a leg up on implementing more
advanced OpenGL features via Ruby.  Also, please remember that this program,
more importantly the attached shaders examples, only support one light.  So 
don't be surprised if you add an OpenGL light and things don't look the way 
you expect.

I hope this code helps you.  Have fun!


Michael


--- Install and execution ---

1) Install Ruby (http://www.ruby-lang.org/en/, only tested with 1.8.6), 
           GLUT (http://www.opengl.org/resources/libraries/glut/), 
           Ruby-OpenGL (http://ruby-opengl.rubyforge.org/) and 
           OpenGL 2.0 or greater (refer to your video card vendor).
2) Extract the ZIP files to a directory of your choice.
3) Within that directory at the command line type "ruby gears_using_shaders.rb"
